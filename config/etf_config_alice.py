import logging

from ncgx.inventory import Hosts, Checks
from vofeed.api import VOFeed

log = logging.getLogger('ncgx')

FLAVOR_MAP = {'ARC-CE': 'arc',
              'HTCONDOR-CE': 'condor'}

ARC_METRICS = (
    'org.sam.ARC-JobSubmit-alice',
)

HTCONDOR_METRICS = (
    'org.sam.CONDOR-JobSubmit-alice',
)

HTCONDOR_STATE_METRICS = (
    'org.sam.CONDOR-JobState-alice',
)


def run(url):
    log.info("Processing vo feed: %s" % url)

    # Get services from the VO feed, i.e
    # list of tuples (hostname, flavor, endpoint)
    feed = VOFeed(url)
    services = feed.get_services()

    # Add hosts, each tagged with corresponding flavors
    # creates /etc/ncgx/conf.d/generated_hosts.cfg
    h = Hosts()
    for service in services:
        h.add(service[0], tags=[service[1]])
    h.serialize()

    # Add corresponding metrics to tags
    # creates /etc/ncgx/conf.d/generated_checks.cfg
    c = Checks()
    c.add_all(ARC_METRICS, tags=["ARC-CE", ])
    c.add_all(HTCONDOR_METRICS, tags=["HTCONDOR-CE", ])

    # Queues
    for service in services:
        host = service[0]
        flavor = service[1]
        endpoint = service[2]
        if flavor not in ["ARC-CE", "HTCONDOR-CE"]:
            continue
        if flavor == 'HTCONDOR-CE':
            # add scitoken to htcondor-ce submissions
            token_path = '/var/lib/gridprobes/alice/scondor/{}/token'.format(host)
            # special handling for HTCONDOR-CE, no queues
            c.add('org.sam.CONDOR-JobState-alice', hosts=(host,),
                  params={'args': {'--resource': 'condor://%s' % host,
                                   '--token': token_path}})
            continue
        if flavor not in FLAVOR_MAP.keys():
            log.warning("Unable to determine type for flavour %s" % flavor)
            continue
        ce_resources = feed.get_ce_resources(host, flavor)

        if ce_resources:
            batch = ce_resources[0][0]
            queue = ce_resources[0][1]
            if not batch:
                batch = "nopbs"
            if flavor == 'ARC-CE':
                # arc2.farm.particle.cz:2811/nordugrid-Condor-grid
                res = "arc://%s/nordugrid/%s/%s" % (endpoint, batch, queue)
        else:
            res = "%s://%s/%s/%s/%s" % (FLAVOR_MAP[flavor], host, 'nosched', 'nobatch', 'noqueue')

        c.add('org.sam.ARC-JobState-alice', hosts=(host,), params={'args': {'--resource': '%s' % res}})

    c.serialize()
