#!/bin/bash

source /usr/bin/etf-init.sh

cat << "EOF"
 _____ _____ _____      _    _     ___ ____ _____
| ____|_   _|  ___|    / \  | |   |_ _/ ___| ____|
|  _|   | | | |_      / _ \ | |    | | |   |  _|
| |___  | | |  _|    / ___ \| |___ | | |___| |___
|_____| |_| |_|     /_/   \_|_____|___\____|_____|
==================================================
EOF

print_header

etf_update

start_xinetd

etf_init

copy_certs

apache_init

config_web_access

config_alerts

init_api

config_htcondor

config_etf

fix_cmk_theme

etf_start

start_oidc_agent

echo "Fetching Alice credentials ..."
su etf -c "/usr/lib/nagios/plugins/globus/refresh_proxy -b 2048 --myproxyuser nagios -H myproxy.cern.ch -t 120 --key /opt/omd/sites/etf/etc/nagios/globus/etf_srv_key.pem --vo alice --lifetime 24 --name NagiosRetrieve-ETF-alice -x /opt/omd/sites/etf/etc/nagios/globus/userproxy.pem-alice --cert /opt/omd/sites/etf/etc/nagios/globus/etf_srv_cert.pem"

echo "Initialising tokens ..."
su - etf -c "oidc-add --pw-file=/opt/omd/sites/etf/.oidc-agent/etf_alice_ce.key etf_alice_ce"
su etf -c "/usr/lib64/nagios/plugins/refresh_token -t 7200 --token-config etf-alice --token-time 345600 --aud /var/lib/gridprobes/alice/scondor/"

etf_wait
