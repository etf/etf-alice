FROM gitlab-registry.cern.ch/etf/docker/etf-exp:qa

LABEL maintainer="Marian Babik <Marian.Babik@cern.ch>"
LABEL description="WLCG ETF ALICE"
LABEL version="1.0"

ENV NSTREAM_ENABLED=0

# Middleware
RUN yum -y install yum-priorities
RUN rpm -ivh http://repository.egi.eu/sw/production/umd/4/centos7/x86_64/updates/umd-release-4.1.3-1.el7.centos.noarch.rpm
RUN rpm -import http://repository.egi.eu/sw/production/umd/UMD-RPM-PGP-KEY
RUN cd /etc/yum.repos.d; wget https://repo.data.kit.edu//data-kit-edu-centos7.repo
#RUN cd /etc/yum.repos.d/ && wget https://research.cs.wisc.edu/htcondor/yum/repo.d/htcondor-stable-rhel7.repo
#COPY ./config/htcondor_stable.repo /etc/yum.repos.d/htcondor-stable-rhel7.repo

# Core
RUN yum -y install voms voms-clients-java oidc-agent-cli

# CONDOR
RUN yum -y install --nogpgcheck condor condor-python 

# CREAM
# RUN yum -y install glite-ce-cream-cli python-suds openldap-clients python-ldap

# ARC 
# Take the pakcages from UMD due to a bug in v6.19.0
#RUN rpm -ivh https://download.nordugrid.org/packages/nordugrid-release/releases/6/centos/el7/x86_64/nordugrid-release-6-1.el7.noarch.rpm
RUN yum -y install nordugrid-arc-client nordugrid-arc-plugins-needed nordugrid-arc-plugins-globus

# ETF Plugins
RUN yum -y install python-jess python-nap nagios-plugins nagios-plugins-globus python-wnfm nagios-plugins-tokens

# Streaming
RUN mkdir -p /var/spool/nstream/outgoing && chmod 777 /var/spool/nstream/outgoing
RUN mkdir /etc/stompclt
COPY ./config/ocsp_handler.cfg /etc/nstream/

# ARC config
RUN mkdir /opt/omd/sites/$CHECK_MK_SITE/.arc
COPY ./config/client.conf /opt/omd/sites/$CHECK_MK_SITE/.arc/
RUN chown -R $CHECK_MK_SITE /opt/omd/sites/$CHECK_MK_SITE/.arc/

# MW env
COPY ./config/grid-env.sh /etc/profile.d/
RUN echo "source /etc/profile.d/grid-env.sh" >> /opt/omd/sites/$CHECK_MK_SITE/.profile

# ETF local checks
COPY ./config/etf_config_alice.py /usr/lib/ncgx/x_plugins/
COPY ./config/wlcg_alice.cfg /etc/ncgx/metrics.d/
RUN mkdir -p /usr/libexec/grid-monitoring/probes/org.alice/wnjob

# ETF config
COPY ./config/alice_checks.cfg /etc/ncgx/conf.d/
COPY ./config/ncgx.cfg /etc/ncgx/

EXPOSE 80 443 6557
COPY ./docker-entrypoint.sh /
ENTRYPOINT /docker-entrypoint.sh
